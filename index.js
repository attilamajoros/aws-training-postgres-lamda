const rp = require("request-promise-native");
let res;

exports.handler = async (event, context, done) => {
    const url = 'http://www.omdbapi.com/?s=%22avengers%22&apikey=10abe927';
    
    console.log(url);
    try {
      res = await rp.get(url);
    } catch (e) {
      console.log('ERR: ', e);
    }
    
    console.log(res);

    const response = {
        statusCode: 200,
        body: res,
        headers: {
            ["Content-Type"]: "application/json"
        },
    };
    
    console.log(response);
    return response;
    };
